import { Request, Response } from "express";
import { ListUserSentComplimentsService } from "../services/ListUserSentComplimentsService";

class ListUserSentComplimentsController {
  async handle(req: Request, res: Response) {
    const listUserSentCompliments = new ListUserSentComplimentsService();
    const user_id = req.user_id;
    const compliments = await listUserSentCompliments.execute(user_id);
    return res.json(compliments);
  }
}

export { ListUserSentComplimentsController };
