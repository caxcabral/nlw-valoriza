import { Request, Response } from "express";
import { ListUserReceivedComplimentsService } from "../services/ListUserReceivedComplimentsService";

class ListUserReceivedComplimentsController {
  async handle(req: Request, res: Response) {
    const listUserReceivedCompliments =
      new ListUserReceivedComplimentsService();
    const user_id = req.user_id;
    const compliments = await listUserReceivedCompliments.execute(user_id);
    return res.json(compliments);
  }
}

export { ListUserReceivedComplimentsController };
