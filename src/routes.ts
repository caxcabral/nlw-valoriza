import { Router } from "express";
import { CreateTagController } from "./controllers/CreateTagController";
import { ensureAdmin } from "../src/middlewares/ensureAdmin";
import { CreateUserController } from "./controllers/CreateUserController";
import { AuthenticateUserController } from "./controllers/AuthenticateUserController";
import { CreateComplimentController } from "./controllers/ComplimentController";
import { ensureAuthenticated } from "./middlewares/ensureAuthenticated";
import { ListUserSentComplimentsController } from "./controllers/ListUserSentComplimentsController";
import { ListUserReceivedComplimentsController } from "./controllers/ListUserReceivedComplimentsController";
import { ListTagsController } from "./controllers/ListTagsController";
import { ListUserController } from "./controllers/ListUserController";

const router = Router();

const userController = new CreateUserController();
const tagController = new CreateTagController();
const authUserController = new AuthenticateUserController();
const complimentController = new CreateComplimentController();
const listUserSentComplimentsController =
  new ListUserSentComplimentsController();
const listUserReceivedComplimentsController =
  new ListUserReceivedComplimentsController();
const listTagsController = new ListTagsController();
const listUserController = new ListUserController();

router.post("/users", userController.handle);
router.post("/tags", ensureAuthenticated, ensureAdmin, tagController.handle);
router.post("/auth", authUserController.handle);
router.post("/compliments", ensureAuthenticated, complimentController.handle);
router.get(
  "/compliments/sent",
  ensureAuthenticated,
  listUserSentComplimentsController.handle
);
router.get(
  "/compliments/received",
  ensureAuthenticated,
  listUserReceivedComplimentsController.handle
);
router.get("/tags", listTagsController.handle);
router.get("/users", ensureAuthenticated, listUserController.handle);

export { router };
