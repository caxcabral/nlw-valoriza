import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";

interface IPayload {
  sub: string;
}

export function ensureAuthenticated(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const authToken = req.headers.authorization;
  if (!authToken) {
    res.status(401).json({ error: "User not authenticated." });
  }

  const [, token] = authToken.split(" ");

  try {
    const { sub } = verify(
      token,
      "c8bda1f72ba2aa9fafc34569d8f4e07f"
    ) as IPayload;

    req.user_id = sub;

    return next();
  } catch (err) {
    console.log(err);
    return res.status(401).end();
  }
}
