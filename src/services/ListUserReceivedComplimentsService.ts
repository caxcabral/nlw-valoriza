import { getCustomRepository } from "typeorm";
import { ComplimentRepository } from "../repositories/ComplimentRepository";

class ListUserReceivedComplimentsService {
  async execute(user_id: string) {
    const complimentsRepositories = getCustomRepository(ComplimentRepository);

    const compliments = await complimentsRepositories.find({
      where: {
        user_receiver: user_id,
      },
    });

    return compliments;
  }
}

export { ListUserReceivedComplimentsService };
