import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories/UserRepository";
import { compare as compareHash } from "bcryptjs";
import { sign } from "jsonwebtoken";

interface IAuthenticateRequest {
  email: string;
  password: string;
}

class AuthenticateUserService {
  async execute({ email, password }: IAuthenticateRequest) {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne({ email });

    if (!user) {
      throw new Error("Email ou senha incorretos");
    }

    const passwordMatch = await compareHash(password, user.password);

    if (!passwordMatch) {
      throw new Error("Email ou senha incorrentos");
    }

    const token = sign(
      { email: user.email },
      "c8bda1f72ba2aa9fafc34569d8f4e07f",
      {
        subject: user.id,
        expiresIn: "1d",
      }
    );

    return token;
  }
}

export { AuthenticateUserService };
