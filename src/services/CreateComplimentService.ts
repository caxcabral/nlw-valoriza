import { getCustomRepository } from "typeorm";
import { ComplimentRepository } from "../repositories/ComplimentRepository";
import { UserRepository } from "../repositories/UserRepository";

interface IComplimentRequest {
  user_receiver: string;
  user_sender: string;
  tag_id: string;
  message: string;
}

class CreateComplimentService {
  async execute({
    user_receiver,
    user_sender,
    tag_id,
    message,
  }: IComplimentRequest) {
    const complimentRepository = getCustomRepository(ComplimentRepository);
    const userRepository = getCustomRepository(UserRepository);

    const userReceiverExists = userRepository.findOne(user_receiver);

    if (user_sender === user_receiver) {
      throw new Error("Can't review yourself");
    }

    if (!userReceiverExists) {
      throw new Error("User Receiver does not exist");
    }

    const compliment = complimentRepository.create({
      tag_id,
      user_receiver,
      user_sender,
      message,
    });

    await complimentRepository.save(compliment);
    return compliment;
  }
}

export { CreateComplimentService };
